# Guide d'utilisation de PlantUML ~ Mouaz MOHAMED

---

## Explication de PlantUML

PlantUML est un outil de création de diagrammes qui utilise une syntaxe textuelle simple et intuitive pour générer des diagrammes UML (Unified Modeling Language). Il utilise une syntaxe basée sur du texte brut pour générer des diagrammes UML.

---

## Utilisation de PlantUML sur Microsoft Visual Studio Code

- Recherchez et installez l'extension PlantUML
- Les fichiers ont généralement une extension `.puml`.
- Utilisez une syntaxe simple et lisible pour définir les éléments du diagramme.
- Utilisez le raccourci `ALT+D` afin de visualiser le diagramme. 

---

## Utilisation de PlantUML sur PlantUML Web Server

- Accèdez à ce lien : [PlantUML Web Server](https://www.plantuml.com/plantuml/uml/)
- Écrivez votre code PlantUML et visualisez en temps réel le résultat de votre diagramme UML
- Vous pourrez par la suite télécharger votre code ou encore partager le code et le diagramme UML via un lien

---

## Exemples de diagrammes PlantUML :
```plantuml
@startuml
left to right direction
:Client:
:Chef_Atelier:
:Mécanicien:
package Servir_Essence{
    Client --> (Remplir le réservoir)
    Chef_Atelier --> (Assurer la gestion)
    Mécanicien --> (Gérer l'entretien du véhicule)
    Chef_Atelier --> (Gérer l'entretien du véhicule)
    note right of (Remplir le réservoir) : Le client prend le pistolet à essence, appui sur la gâchette et rempli son réservoir de carburant
}
@enduml 
```

---

```plantuml
@startuml
left to right direction
:Agent_de_Voyage:
:Client:
package Agence_de_Voyage{
    Agent_de_Voyage --> (Organiser le voyage)
    (Organiser un voyage) ..> (Réserver un chambre d'hôtel) : <<include>>
    (Organiser un voyage) ..> (Réserver un taxi) : <<include>>
    (Organiser un voyage) ..> (Réserver un billet) : <<include>>
    Client --> (Demander une facture)
    (Réserver un billet) <|-d- (Avion)
    (Réserver un billet) <|-d- (Train)
    (Organiser un voyage) <..(Établir une facture) : <<extends>>
}
@enduml
```

---

```plantuml
@startuml
left to right direction
:Client:
package Magasin_Location_Video{
    Client --> (Emprunter une vidéo)
    Client --> (Restituer une vidéo)
    (Emprunter une vidéo) ..> (Rechercher une vidéo) : <<inclure>>
    (Rechercher une vidéo) <|-- (Par genre)
    (Rechercher une vidéo) <|-- (Par titres de films)
    
}
@enduml
```

---

```plantuml
@startuml 
left to right direction
:Employé:
package Mediathèque{
    Employé --> (Gestion des adhérents)
    Employé --> (Gestion des oeuvres et des médiathèques)
}
@enduml 
```

---

```plantuml
@startuml
left to right direction
:Client:
package RetraitGAB {
    Client ----> (Retrait)
    Client --> (Dépôt)
    Client --> (Virement)
    Client --> (Consulter)
    (Retrait) ..> (Authentification) :<<include>>
    (Dépôt) ..> (Authentification) :<<include>>
    (Virement) ..> (Authentification) :<<include>>
    (Vérifier le solde) <.. (Retrait) :<<extends>>
    (Vérifier le solde) <.. (Virement) :<<extends>>
    (Consulter) <|-d- (Site Web)
    (Consulter) <|-d- (Guichet)
    (Consulter) <|-d- (Relevé de compte format papier)
    (Consulter) .d.> (Authentification) :<<include>>
}
@enduml
```